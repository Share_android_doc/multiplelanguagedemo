package com.example.kimsoerhrd.localizedemo;

import android.content.res.Configuration;
import android.support.v7.app.AppCompatActivity;

import java.util.Locale;

public class LocalizeHelper {

    AppCompatActivity activity;

    LocalizeHelper(AppCompatActivity activity) {
        this.activity = activity;
    }

    void setLanguage(String language){
        resetLanguage(language);
    }

    void resetLanguage(String language){
        Locale locale = new Locale(language);
        Configuration configuration = activity.getResources().getConfiguration();
        configuration.setLocale(locale);
        activity.getResources().updateConfiguration(configuration, activity.getResources().getDisplayMetrics());
        activity.recreate();
    }
}
