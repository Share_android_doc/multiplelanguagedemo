package com.example.kimsoerhrd.localizedemo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class LoginActivity extends AppCompatActivity {
    EditText username, password;

    LocalizeHelper localizeHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        localizeHelper= new LocalizeHelper(this);
        username = findViewById(R.id.etUsername);
        password = findViewById(R.id.etPassword);

    }

    public void onHome(View view) {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    public void onKhmerLanguage(View view) {
        localizeHelper.setLanguage("km");
    }

    public void onEnglish(View view) {
        localizeHelper.setLanguage("en");
    }
}
